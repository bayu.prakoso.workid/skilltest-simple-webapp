# Skill Test - Simple Web App

In this part of the test, I made some text manipulation web app to do reverse the input string and do undo or redo based on how many clicks .

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
